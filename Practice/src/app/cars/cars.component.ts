import { Component, OnInit , ViewEncapsulation, ViewChild, ElementRef, AfterViewInit} from '@angular/core';

@Component({
  selector: 'app-cars',
  // selector: '[app-cars]',
  // selector: '.app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CarsComponent implements OnInit, AfterViewInit {
  @ViewChild('anotherDiv',{static: true}) anotherDiv: ElementRef;
  @ViewChild('carComponent', {static:true}) carComponent: Component;
  carValues:{name:string, model:string, color:string}[] = [
    {name:'Ford', model:'Nova', color:'Blue'},
    {name:'chevy ', model:'G-wagen', color:'Black'},
    {name:'merc', model:'C100', color:'pink'}
  ]

  carWasSubmitted(carData: {name:string, model:string, color:string}){
    console.log(carData)
  }


  constructor() { }

  ngOnInit() {
    // console.log(this.anotherDiv.nativeElement)

  }

  ngAfterViewInit(){
    // console.log(this.carComponent.carValues)
  }

}
