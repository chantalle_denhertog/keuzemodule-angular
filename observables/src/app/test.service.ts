import { EventEmitter } from "@angular/core";

export class TestService {
  showMessage = new EventEmitter<string>();
}