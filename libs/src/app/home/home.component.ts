import { Component, OnInit } from '@angular/core';
// import { ButtonsModule } from 'ngx-bootstrap/buttons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  disabled = false;
  singleModel = true;
  cookieModel = 'Chocolate';

  showError = true;
  defaultAlert = {
    type: 'danger',
    timeout: 2000
  };

  constructor() { }
  ngOnInit() {
    // setTimeout(() => {
    //   this.defaultAlert.type = 'success';
    // }, 2000);
  }

  onClosed(event) {
    console.log(event);
  }


}
