import { PipeTransform , Pipe } from '@angular/core';

@Pipe({
    name: 'mycustom'
})
export class CustomPipe implements PipeTransform {
    transform(value: number, toSum: number, title: string) {
        if ( toSum > 10) {
            return `${title} =  ${value + toSum}`;
        }
        return value;
    }
}