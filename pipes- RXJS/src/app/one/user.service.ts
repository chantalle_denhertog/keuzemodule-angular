import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap , map } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';

export interface UserData {
    name: string;
    email: string;
}


@Injectable({
    providedIn: 'root'
})
export class UserService {
    subject = new BehaviorSubject(null);
    constructor(
        private http: HttpClient
    ) {}

    getUser() {
        return this.http.get('https://jsonplaceholder.typicode.com/users/1')
        .pipe(
            tap(
                data => this.subject.next(data),
                error => console.log(error)
            ),
            map( (data: UserData) => {
                return data = {
                    name: data.name,
                    email: data.email
                }
            })
            
        ) 
    }

    storeUser(data){
       console.log('User store', data)
    }

}