import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {
  user = {
    name: 'Francis',
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    firstLogin: 'Mon Dec 6 2021 00:00:00 GMT-0416',
    bignumber: '2.718281828459045',
    perc: 0.259,
    dad: {
      name: 'Steve',
      lastname: 'Jones',
      age: 50
    },
    json:  {name: 'Francis', lastname: 'Jones', nested: {xyz: 3, numbers: [1, 2, 3, 4, 5]}},
  };

  userToFilter = '';
  customValue = 20;

  greeting = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Hello guys!!');
    }, 2000);
  });

  listOfUsers =  [
    {
      name: 'Francis',
      lastname: 'Jones',
      age: 25
    },
    {
      name: 'Steve',
      lastname: 'Mills',
      age: 30
    },
    {
      name: 'Katherine',
      lastname: 'Robertson',
      age: 60
    },
    {
      name: 'Lisa',
      lastname: 'Kudrow',
      age: 19
    }
  ];

  constructor(
  ) { }

  ngOnInit() {
  }

}
