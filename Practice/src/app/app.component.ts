import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css']
})
export class AppComponent implements OnInit {
  artist: string = 'Hendrix';
  show: boolean = true;

coffees:{style: string, available:string} []=[
  {style:'flat white', available:'yes'},
  {style:'espresso', available:'no'},
  {style:'cappucino', available:'yes'},
  {style:'black coffee', available:'no'}
]

items = [
  'yes',
  'no',
  'not sure',
  'maybe'
]

  // list = ['1', '2', '3']

  styles = {
    'font-size':'20px',
    'color':'pink'
  }

  classes = {
    'class-name1':true,
    'class-name2':true
  }

  getColor(){
    return 'yellow';
  }

  changeArtist(){
    this.artist = 'Santana';
  }

ngOnInit(){
  setTimeout(()=>{
    this.show = true;
  }, 3000)
}
}

