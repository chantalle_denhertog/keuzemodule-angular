import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';

import { PostsService } from './posts.service';

interface Post {
  title: string;
  body: string;
  id?: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f', {static: true}) ourForm: NgForm;
  postsList: Post[] = [];

  constructor(
    private http: HttpClient,
    private postsService:PostsService
  ) {}

  submitHandler() {
    const postData = this.ourForm.value;
    this.postsService.addPost(postData).subscribe((data: Post)=>{
      this.postsList.push(data)
    });
   }

   getPosts() {
    this.postsService.getPosts().subscribe((data: Post[])=>{
     this.postsList = data;
    }) 
  }

  deletePost(id: number) {
     this.postsService.deletePost(id).subscribe(data =>{
       this.getPosts();
     })

  }

  ngOnInit() {
    this.getPosts();
  }





















  //  Zonder Service
  // getPosts() {
  //   // let postParams = new HttpParams();
  //   // postParams = postParams.append('_order', 'desc');
  //   // postParams = postParams.append('_sort', 'id');

  //   const postParams = new HttpParams({fromString: '_order=asc&_sort=id'});

  //   this.http.get(
  //     'http://localhost:3004/posts',
  //     {
  //       params: postParams,
  //       // observe: 'body'
  //       observe: 'events'
  //     }
  //     ).subscribe((event: any) => {
  //     // this.postsList = response.body;
  //     if(event.type === HttpEventType.Sent){
  //       console.log('started')
  //     }
  //     if(event.type === HttpEventType.Response){
  //       console.log('DONE');
  //       this.postsList = event.body;
  //     }
  //   });
  // }

  // deletePost(id: number) {
  //   this.http.delete(`http://localhost:3004/posts/${id}`).subscribe(response => {
  //     this.getPosts();
  //   })
  //  }

  // ngOnInit(){
  //   this.getPosts();
  // }

}
