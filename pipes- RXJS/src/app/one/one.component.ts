import { Component, OnInit , OnDestroy} from '@angular/core';
import { UserService , UserData } from './user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit, OnDestroy {
  user = null;
  userSubject: Subscription; 
  constructor(
    private userService: UserService
  ) { }

  removeUser() {
    this.userService.subject.next(null)
  }

  ngOnInit() {
   this.userSubject = this.userService.subject.subscribe((data)=>{
    this.user = data;
   })
  }

  ngOnDestroy(){
    this.userSubject.unsubscribe();
  }

}
