import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes,
  group,
  useAnimation
} from '@angular/animations';
import { bounce, shake} from 'ng-animate';

const firstAnim = trigger('first',[
  state('green',style({
    'background-color':'green',
    width: '100%'
  })),
  state('blue',style({
    'background-color':'blue',
    width: '20%'
  })),
  transition('green <=> blue',animate(500)),
]);

const secondAnim = trigger('second',[
  state('on',style({
    opacity: 1
  })),
  state('off',style({
    opacity: 0
  })),
  transition('on <=> *', [
    style({
      'background-color':'blue'
    }),
    animate(1000,style({
      height:'200px'
    })),
    animate(2000)
  ])
])

const thirdAnim =  trigger('third',[
  state('in',style({
    opacity:1,
    transform:'translateX(0)'
  })),
  transition('void => *',[
    style({
      opacity:0,
      transform:'translateX(-200px)'
    }),
    animate(500)
  ]),
  transition('* => void',[
    animate(500,style({
      opacity:0,
      transform:'translateX(200px)'
    }))
  ])
])

const fourthAnim = trigger('fourth',[
  state('off',style({
    backgroundColor: 'red'
  })),
  state('on',style({
    backgroundColor: 'blue'
  })),
  transition('* => *',[
    animate(500,keyframes([
      style({height:'100px', offset: 0.7 }),
      style({height:'200px', offset: 0.8 }),
      style({height:'100px', offset: 0.9 })
    ]))
  ])
])


const groupFourthAnim = trigger('fourth',[
  state('off',style({
    backgroundColor: 'red'
  })),
  state('on',style({
    backgroundColor: 'blue'
  })),
  transition('* => *',[
    group([
      animate(2000,keyframes([
        style({height:'100px', offset: 0.2 }),
        style({height:'200px', offset: 0.5 }),
        style({height:'100px', offset: 0.7 })
      ])),
      animate(2000,keyframes([
        style({width:'50%', offset: 0.2 }),
        style({width:'80%', offset: 0.5 }),
        style({width:'100%', offset: 0.7 })
      ]))
    ])
  ])
])

const bounceAnim = trigger('bounce',[
  transition('false => true',useAnimation(bounce,{
    params:{delay: 2}
  })),
  transition('true => false',useAnimation(shake,{
    params:{timing: 2}
  })),
])

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    firstAnim,
    secondAnim,
    thirdAnim,
    // fourthAnim,
    trigger('parent', [
      transition(':enter', [])
    ]),
    groupFourthAnim,
    bounceAnim

  ]
})
export class AppComponent {
  firstState = 'green'
  secondState = 'on';
  fourthState = 'off';
  bounce = true;
  listOfNames =  ['francis', 'ron' ];

  bounceAnimChange(){
    if(this.bounce){
      this.bounce = false
    } else {
      this.bounce = true
    }
  }

  fourthAnimChange(){
    if(this.fourthState === 'on'){
      this.fourthState = 'off'
    } else {
      this.fourthState = 'on'
    }
  }

  secondAnimChange(){
    if(this.secondState === 'on'){
      this.secondState = 'off'
    } else {
      this.secondState = 'on'
    }
  }


  firstAnimChange(){
    if(this.firstState === 'green'){
      this.firstState = 'blue'
    } else {
      this.firstState = 'green'
    }
  }

  onAnimationEvent(event:AnimationEvent ){

    // trigger name
    //console.log(`Trigger name: ${event.triggerName}`)

    // phase name
    //console.log(`Trigger name: ${event.phaseName}`)

    // time
    //console.log(`Trigger name: ${event.totalTime}`)

    // from 
    // console.log(`Trigger name: ${event.fromState}`)

    // to 
    // console.log(`Trigger name: ${event.toState}`)

    // HTML
    //console.log(event.element)
    //event.element.innerHTML = 'hey'

  }

  addName(name) {
    this.listOfNames.push(name);
  }

  deleteItemlist(element: number) {
    this.listOfNames.splice(element, 1);
  }

}
