import { Component, OnInit } from '@angular/core';
import { of , merge, fromEvent, interval, Observable , throwError } from 'rxjs';
import { mergeMap, map , exhaustMap, take, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.css']
})
export class TwoComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    interval(1000).pipe(
      mergeMap( x => {
        if(x > 2) {
          return throwError(false)
        } 
        return of(x)
      }),
      catchError( val => {
        return of('All work and no play makes jack a dull boy');
      })
    ).subscribe(
      x => console.log(x),
      e => console.log(e)
    )

    // const request = new Observable((observer)=>{
    //   setTimeout(()=>{
    //     observer.error('USER DATA')
    //   }, 2000)
    // });


    // const click = fromEvent(document,'click');
    // const result = click.pipe(
    //   exhaustMap( ev => request.pipe(
    //     take(1),
    //     map( user => console.log(user))
    //   ))
    // );

    // result.subscribe();


    // const click = fromEvent(document,'click');
    // const numbers = of(1,3,5);
    // const letters = of('a','b','c');
    // const both = merge(numbers,letters,click)

    // both.subscribe( x => console.log(x));

    // const numbers = of(1,3,5);
    // const result = numbers.pipe(
    //   mergeMap( 
    //     (x)=> of('a','b','c').pipe(
    //       map( i => {
    //         return x + i
    //       })
    //     )
    //   )
    // )

    // result.subscribe(
    //   x => console.log(x)
    // )


  }

}
