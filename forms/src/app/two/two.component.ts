import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators , FormArray , FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.css']
})
export class TwoComponent implements OnInit {
  // personalForm: FormGroup;
  workHours = [4,6,8,12];
  abilitiesArray = [ 'javascript', 'angular', 'react'];

  personalForm = this.fb.group({
    userProfile: this.fb.group({
      name: [null, this.customValidation],
      email: [null, [Validators.required, Validators.email]]
    }),
    work: [8],
    status: [null],
    abilities: this.fb.array([
      this.fb.control(''),
      this.fb.control(''),
      this.fb.control('')
    ])
  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    // this.personalForm = new FormGroup({
    //   userProfile: new FormGroup({
    //     name: new FormControl(null, this.customValidation),
    //     email: new FormControl(null, [Validators.required, Validators.email])
    //   }),
    //   work: new FormControl(8),
    //   status: new FormControl(null),
    //   abilities: new FormArray([
    //     new FormControl(),
    //     new FormControl(),
    //     new FormControl()
    //   ])
    // });

    // this.personalForm.valueChanges.subscribe(
    //   (values) => console.log(values)
    // );

    // this.personalForm.statusChanges.subscribe(
    //   (values) => console.log(values)
    // );

    // this.personalForm.setValue({
    //   userProfile:{
    //     name...
    //   }
    // })

    this.personalForm.patchValue({
      userProfile:{
        name:'francis'
      }
    });


  }

  customValidation(control: FormControl ){
    if ( control.value === null || control.value === '') {
      return { noNameProvided: true };
    }
    return null;
  }

  submitForm(){
    console.log(this.personalForm)
  }

}
