import { Component, OnInit } from '@angular/core';

import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
 // providers: [UserService]
})
export class AppComponent implements OnInit {

  constructor(
    private userService: UserService
  ){}

  users: {name: string }[] = [];

  // onUserRemoved( toRemove: {position: number}) {
  //   this.users.splice(toRemove.position, 1);
  //   this.userService.consoleSomething()
  // }

  // onAddUser(user: { name: string }) {
  //   this.users.push(user);
  //   this.userService.consoleSomething()
  // }

  ngOnInit(){
    this.users = this.userService.users
  }

}
