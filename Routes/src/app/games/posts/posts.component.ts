import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params} from '@angular/router'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  post: { id: number, rating: string};
  related: { id: number, rating: string} = {
    id: 2836532,
    rating: 'b'
  };

  constructor(
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    console.log(this.activatedRoute);
    const id = 'id';
    const rating = 'rating';
    this.post = {
      id: this.activatedRoute.snapshot.params[id],
      rating: this.activatedRoute.snapshot.params[rating]
    };

    this.activatedRoute.params.subscribe( (params: Params) => {
      this.post.id = params[id];
      this.post.rating = params[rating];
    });

    console.log(this.activatedRoute.snapshot.data['title']);
    this.activatedRoute.data.subscribe(data => console.log(data['title']));

  }

}
