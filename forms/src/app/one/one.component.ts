import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit {
@ViewChild('f', {static: true}) ourForm: NgForm
ageOptions = ['20-30', '30-40', '40-50', '>60'];
message = '';
  // user = {
  //   name:'jjsjss',
  //   lastname:'skjshs'
  // }

  // state = { 
  //   userData:{
  //     name:'',
  //     email:''
  //   },
  //   age:'',
  //   message:'',
  //   newsletter:''
  // }

  constructor() { }

  submitHandler(form: NgForm){
    // if(this.ourForm.valid){
      console.log(this.ourForm.value.age)
      const userData = {
        age: this.ourForm.value.age
      };

      this.ourForm.form.reset();
    // }
  }

  ngOnInit() {
    setTimeout(()=>{

      // this.ourForm.form.patchValue({
      //   userProfile: {
      //     name: 'Francis'
      //   }
      // });

      this.ourForm.form.setValue({
        userProfile: {
          name: 'Francis',
          email: 'francis@gmail.com'
        },
        age: '20-30',
        message: 'Mesage again',
        newsletter: true
      });

    }, 2000)
  }

}
