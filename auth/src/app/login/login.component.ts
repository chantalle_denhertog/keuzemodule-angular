import { Component, OnInit , ViewChild  } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService, UserToken } from './../lib/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('f', {static: true}) loginRegisterForm: NgForm;
  formMode = true; // true = register
  hasError: string = null;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    setTimeout(() => {
      // This is for later, so we don't have to enter user and pass every time
      this.loginRegisterForm.form.setValue({
        email: 'francis@gmail.com',
        password: 'testing123',
      });
    }, 1000);
  }

  toggleMode() {
    this.formMode = !this.formMode;
  }

  submitHandler() {
    const formValues = this.loginRegisterForm.value;

    if (this.loginRegisterForm.valid) {
      if (this.formMode) {
        // REGISTER
        this.authService.register(formValues).subscribe(
          data => this.hadleSuccess(data),
          error => this.handleError(error)
        );
      } else {
        // LOGIN
        this.authService.login(formValues).subscribe(
          data => this.hadleSuccess(data),
          error => this.handleError(error)
        );
      }
      this.loginRegisterForm.form.reset();
    }
  }


  hadleSuccess(data: UserToken) {
    this.router.navigate(['./']);
    this.hasError = null;
  }

  handleError(error: string) {
    this.hasError = error;
  }

}
